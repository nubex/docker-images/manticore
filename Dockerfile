FROM manticoresearch/manticore:4.2.0

RUN mkdir -p /app/data /app/dicts && chmod -R 777 /app

COPY ./manticore.conf /etc/manticoresearch/manticore.conf
COPY ./dicts          /app/dicts

VOLUME ["/app/data"]

STOPSIGNAL SIGKILL
EXPOSE 9306

